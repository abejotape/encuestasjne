"use strict"

window.fn = {}

// Para mostrar mensaje de error
// window.fn.alert.create("wc-message")

// Para ocultar mensaje de error
// window.fn.alert.remove("wc-message")


fn.alert = {
    create : function(context, modal){
        let icon, tmpInline, tmpModal,WcMessage,type, text;

        if(modal == 'inline' || modal == undefined){
            WcMessage = document.querySelector(context);
            type = WcMessage.getAttribute('type');
            text = WcMessage.getAttribute('text');
        }else{
            type = context.type
        }       

        switch(type){
            case "info":
                icon = 'info'
                break;
            case "alert":
                icon = 'warning'
                break;
            case "success":
                icon = 'check_circle'
                break;
            default:
        }

        if(modal == 'inline' || modal == undefined){
            tmpInline = `<div class="message message-`+type+` cleaner">
                            <i class="material-icons message--is-icon">`+icon+`</i>
                            <span class="message--is-txt">`+text+`</span>
                        </div>`

            WcMessage.insertAdjacentHTML('afterbegin', tmpInline);
        }else{

            tmpModal = `<wc-alert class="alert alert--is-`+context.type+` ">
                            <div class="alert-content ">
                                <div class="alert-content-icon">
                                    <img src="../../images/icons/`+context.type+`.svg" alt="">
                                </div>
                                <div class="alert-content-title">`+context.title+`</div>
                                <div class="alert-content-desc">`+context.desc+`</div>
                                <div class="alert-content-button">
                                    <wc-modal-cancel class="button button-terciary" >Cancelar</wc-modal-cancel>
                                    <wc-modal-ok class="button button-primary" >Aceptar</wc-modal-cancel>
                                </div>
                            </div>
                        </wc-alert>`

            let body = document.querySelector('body');
            body.insertAdjacentHTML('afterbegin', tmpModal);

            setTimeout(function(){
                document.getElementsByTagName('body')[0].classList.add('modal--is-open')
            }, 100)


            document.querySelector("wc-modal-cancel").addEventListener("click", function(e){
                e.preventDefault()
                context.cancel()
                document.getElementsByTagName('body')[0].classList.remove('modal--is-open')
                setTimeout(function(){
                    document.querySelectorAll('wc-alert').forEach(el => el.remove());
                }, 800)

            })

            document.querySelector("wc-modal-ok").addEventListener("click", function(e){
                e.preventDefault()
                context.ok()
                document.getElementsByTagName('body')[0].classList.remove('modal--is-open')
                setTimeout(function(){
                    document.querySelectorAll('wc-alert').forEach(el => el.remove());
                }, 800)
            })


        }
        
    },
    remove : function(context){
        document.querySelector(context).firstChild.remove();
    }
}

fn.modal = {
    close : function(customElement){           
        document.querySelector(customElement).classList.remove('modal-content--is-open')
        document.getElementsByTagName('body')[0].classList.remove('modal--is-open')
    },
    open: function(customElement){

        if(document.querySelector("wc-modal-bg") == null ){
            let body = document.querySelector('body');
            body.appendChild(document.createElement('wc-modal-bg'));
        }

        let modal = document.querySelector(customElement);
	    modal.classList.add('modal-content--is-open')
        document.getElementsByTagName('body')[0].classList.add('modal--is-open')
    }
}

fn.actions = {
    hide : function(ev){
        ev.target.classList.remove('table-options-ul--is-open')
    }
}

fn.profile = {    
    show : function(customElement){
        let menuProfile = document.querySelector(customElement);

        if(menuProfile.classList.contains('header-profile--is-open'))
            menuProfile.classList.remove('header-profile--is-open')
        else
            menuProfile.classList.add('header-profile--is-open')
    },
    hide : function(ev){
        ev.target.classList.remove('header-profile--is-open')
    }
}

fn.notification = {
    show : function(customElement){
        let menuProfile = document.querySelector(customElement);

        if(menuProfile.classList.contains('header-notification--is-open'))
            menuProfile.classList.remove('header-notification--is-open')
        else
            menuProfile.classList.add('header-notification--is-open')
    },
    hide : function(ev){
        ev.target.classList.remove('header-notification--is-open')
    }
}



// Colapsar Sidebar
fn.sidebar = {
    open : function(){

    }
}
