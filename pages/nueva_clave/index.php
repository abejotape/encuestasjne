<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="inbox.css">
    <title>RESUMEN</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="rclave-section">                    
            <div class="inbox-header">
                <div class="layout-title">Nueva Clave</div>
            </div>   
            <p class="nuevaclave-section-p">(La nueva contraseña deberá contener solo mayúsculas, minúsculas  y números)</p>
            <form class="" action="index.html" method="post">
                  <label for="" class="rclave-label">Código de seguridad</label>
                  <input type="text" name="" value="" class="rclave-input"> 
                  <span class="nuevaclave-form-span">0/6</span>
                  <label for="" class="rclave-label">Nueva contraseña</label>
                  <input type="text" name="" value="" class="rclave-input"> 
                  <span class="nuevaclave-form-span">0/20</span>
                  <label for="" class="rclave-label">Confirmar nueva contraseña </label>
                  <input type="text" name="" value="" class="rclave-input"> 
                  <span class="nuevaclave-form-span">0/20</span>
                  <div class="rclave-boton">
                      <button type="button" name="button" class="button button-secondary">Volver</button>
                      <button type="button" name="button" class="button button-primary">Guardar</button>
                  </div> 
            </form>                   
        </section>
    </main>

    <script src="../../js/app.js"></script>
    <script src="inbox.js"></script>
    
</body>
</html>