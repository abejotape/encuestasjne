<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>BANDEJA DE EXPEDIENTES</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="inbox cleaner">
                    <div class="inbox-header">
                        <div class="layout-title">Bandeja de Expedientes</div>
                        <div class="inbox-header-button"><i class="inbox-header-button--is-icon material-icons">label</i><p>Etiquetar</p></div>
                        <div class="inbox-header-button"><i class="inbox-header-button--is-icon material-icons">youtube_searched_for</i><p>Filtrar</p></div>
                        <div class="inbox-header-button"><i class="inbox-header-button--is-icon material-icons">cloud_download</i><p>Exportar</p></div>
                        <div onclick="(function(){ fn.modal.open('wc-modal-crear-documento') })()" class="inbox-header-button inbox-header-button--is-send cleaner"><i class="inbox-header-button--is-icon material-icons">add</i><p>Nuevo Documento</p></div>
                    </div>
                    <div class="inbox-body">
                        <table class="inbox-table cleaner">
                            <thead class="inbox-table-thead">
                                <tr class="inbox-table-thead-tr">
                                    <td class="inbox-table-thead-td">
                                        <label for="checkbox-test" class="form-checkbox">
                                            <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                        </label>
                                    </td>
                                    <td class="inbox-table-thead-td">N° Exp</td>
                                    <td class="inbox-table-thead-td">Asunto</td>
                                    <td class="inbox-table-thead-td">Tipo y materia</td>
                                    <td class="inbox-table-thead-td">Fecha registro</td>
                                    <td class="inbox-table-thead-td">Ubigeo</td>
                                    <td class="inbox-table-thead-td">Estado</td>
                                    <td class="inbox-table-thead-td">...</td>
                                </tr>
                            </thead>
                            <tbody class="inbox-table-tbody">

                                <!-- Fila con su detalle -->
                                <tr class="inbox-table-tbody-tr inbox-table-odd">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                            <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">ERM.2018</a></td>
                                    <td class="inbox-table-tbody-td">Registro de encuestadora</td>
                                    <td class="inbox-table-tbody-td">Encuestadora / Inscripción</td>
                                    <td class="inbox-table-tbody-td">21/01/2019</td>
                                    <td class="inbox-table-tbody-td">Loreto/Dato del marañon</td>
                                    <td class="inbox-table-tbody-td">Registrado</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()" >
                                            <li class="table-options-li"><i class="material-icons table-options-li--is-icon">edit</i>Editar</li>
                                            <li class="table-options-li"><i class="material-icons table-options-li--is-icon">delete</i>Borrar</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="inbox-table-details inbox-table-odd">
                                    <td class="inbox-table-details-td" colspan="8">
                                        <table class="table-details">
                                            <thead>
                                                <tr class="table-details-thead-tr">
                                                    <td class="table-details-thead-td">Código</td>
                                                    <td class="table-details-thead-td">Título</td>
                                                    <td class="table-details-thead-td">Tipo doc</td>
                                                    <td class="table-details-thead-td">Fec. Declaración</td>
                                                    <td class="table-details-thead-td">Fec. Incorporación</td>
                                                    <td class="table-details-thead-td">Orden</td>
                                                    <td class="table-details-thead-td">Pag. Ini</td>
                                                    <td class="table-details-thead-td">Pag. Fin</td>
                                                    <td class="table-details-thead-td">Origen</td>
                                                    <td class="table-details-thead-td">..</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="table-details-tbody-tr">
                                                    <td class="table-details-tbody-td">20180009876-HCYO</td>
                                                    <td class="table-details-tbody-td">Medios probatorios</td>
                                                    <td class="table-details-tbody-td">Notificación</td>
                                                    <td class="table-details-tbody-td">21/04/2012</td>
                                                    <td class="table-details-tbody-td">21/04/2012</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                    <td class="table-details-tbody-td">1</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                    <td class="table-details-tbody-td">Electrónico</td>
                                                    <td class="table-details-tbody-td"> <a href="javascript:void(0)"><i class="table-icons--is-icon material-icons">link</i></a> </td>
                                                </tr>
                                                <tr class="table-details-tbody-tr">
                                                    <td class="table-details-tbody-td">20180009876-HCYO</td>
                                                    <td class="table-details-tbody-td">Medios probatorios</td>
                                                    <td class="table-details-tbody-td">Notificación</td>
                                                    <td class="table-details-tbody-td">21/04/2012</td>
                                                    <td class="table-details-tbody-td">21/04/2012</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                    <td class="table-details-tbody-td">1</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                    <td class="table-details-tbody-td">Electrónico</td>
                                                    <td class="table-details-tbody-td"> <a href="javascript:void(0)"><i class="table-icons--is-icon material-icons">link</i></a> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- end Fila con su detalle -->

                                <tr class="inbox-table-tbody-tr inbox-table-even">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                            <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">ERM.2018</a></td>
                                    <td class="inbox-table-tbody-td">Registro de encuestadora</td>
                                    <td class="inbox-table-tbody-td">Encuestadora / Inscripción</td>
                                    <td class="inbox-table-tbody-td">21/01/2019</td>
                                    <td class="inbox-table-tbody-td">Loreto/Dato del marañon</td>
                                    <td class="inbox-table-tbody-td">Registrado</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()">
                                            <li class="table-options-li">Editar</li>
                                            <li class="table-options-li">Borrar</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="inbox-table-details inbox-table-even">
                                    <td class="inbox-table-details-td" colspan="8">
                                        <table class="table-details">
                                            <thead>
                                                <tr class="table-details-thead-tr">
                                                    <td class="table-details-thead-td">Exp</td>
                                                    <td class="table-details-thead-td">Fecha</td>
                                                    <td class="table-details-thead-td">Hora</td>
                                                    <td class="table-details-thead-td">Detalle</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="table-details-tbody-tr">
                                                    <td class="table-details-tbody-td">20180009876-HCYO</td>
                                                    <td class="table-details-tbody-td">10/10/1987</td>
                                                    <td class="table-details-tbody-td">01:10:10</td>
                                                    <td class="table-details-tbody-td">Informe de encuestas</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </section>
    </main>

    <!-- Modal Crear documento -->
    <wc-modal-crear-documento class="modal-content" >
        <form action="" class="formulario">
            <a onclick="(function(){ fn.modal.close('wc-modal-crear-documento') })()" class="modal-content--is-close cleaner" href="javascript:void(0)"><i class="material-icons modal-content-icon--is-close">cancel</i></a>
    
            <div class="modal-head cleaner">
                <strong class="modal-head-title">Crear Documento</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid cleaner">
                    <div class="col-sm-6 cleaner">
                        <div class="form-double cleaner">
                            <div class="form-double-search cleaner">
                                <label for="User" class="form-label" required>RUC:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                            <button class="button button-terciary" >Validar</button>
                        </div>
                    </div>
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label" required>Razón Social:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">                
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Correo electrónico:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Número de celular:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">
                    <div class="col-sm-6 cleaner">
                        <div class="form-double cleaner">
                            <div class="form-double-search cleaner">
                                <label for="User" class="form-label">DNI Rep. legal:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                            <button class="button button-terciary" id="ValidarDNI">Validar</button>
                        </div>
                    </div>
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Representante Legal:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Logo empresa: ( Max 400kb .jpg )</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="departamento" class="form-label">Pregunta secreta:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Elegir--</option>
                                    <option value="1">Nombre de mascota</option>
                                    <option value="1">Colegio de secundaria</option>
                                </select>
                            </label>
                        </div>
                    </div>
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Respuesta de pregunta:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-content cleaner">
                <button onclick="(function(){ fn.modal.close('wc-modal-crear-documento') })()" type="button" class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                <button type="button" class="button button-primary" ><i class="material-icons">highlight_off</i>Guardar</button>
            </div>
        </form>
    </wc-modal-crear-documento>
    
    
    <?php include '../_include/footer.php' ?>    
</body>
</html>