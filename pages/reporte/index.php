<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="reporte.css">
    <title>REPORTE DE PROFESIONALES ESTADÍSTICOS</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside_prof.php") ?>
            <section class="layout-content">
                <div class="titles-content">
                    <div class="layout-title">Reporte de profesionales estadísticos</div>
                </div>               
                <div class="inbox cleaner">
                    <div class="inbox-header--is-filter">
                        <div class="cleaner">
                            <label for="departamento" class="form-label reporte-estado">Estado:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Elegir--</option>
                                    <option value="1">Abierto</option>
                                    <option value="1">Cerrado</option>
                                </select>
                            </label>
                        </div>
                        <div class="cleaner m-l-10">
                            <label for="departamento" class="form-label">Desde - Hasta:</label>
                            <div class="cleaner form-calendars">
                                <div class="form-calendars--is-one">
                                    <i class="form-calendars--is-icon material-icons">calendar_today</i>
                                    <input type="text" class="form-input" id="calendario-desde" placeholder="dd-mm-yyyy">
                                </div>

                                <div class="form-calendars--is-two">
                                    <i class="form-calendars--is-icon material-icons">calendar_today</i>
                                    <input type="text"  class="form-input" id="calendario-hasta" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                            
                        </div>
                        
                        <button type="button" class="button button-primary button--is-filter">Buscar</button>
                        <button type="button" class="button button-secondary button--is-filter">Imprimir</button>
                    </div>
                    
                    <div class="inbox-body">
                        <table class="inbox-table cleaner">
                            <thead class="inbox-table-thead">
                                <tr class="inbox-table-thead-tr">
                                    <td class="inbox-table-thead-td">N° col</td>
                                    <td class="inbox-table-thead-td">DNI</td>
                                    <td class="inbox-table-thead-td">Nombres</td>
                                    <td class="inbox-table-thead-td">Apellidos</td>
                                    <td class="inbox-table-thead-td">Sede</td>
                                    <td class="inbox-table-thead-td">Habilitado</td>
                                    <td class="inbox-table-thead-td">Ult. vigencia</td>
                                    <td class="inbox-table-thead-td">Carrera</td>
                                </tr>
                            </thead>
                            <tbody class="inbox-table-tbody">
                                <tr class="inbox-table-tbody-tr inbox-table-odd">
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">10019</a></td>
                                    <td class="inbox-table-tbody-td">45322111</td>
                                    <td class="inbox-table-tbody-td">Daniel</td>
                                    <td class="inbox-table-tbody-td">Acevedo Jhong</td>
                                    <td class="inbox-table-tbody-td">la Libertad</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">02.2019 a 08.2019</td>
                                    <td class="inbox-table-tbody-td">ING. ESTADÍSTICA</td>
                                    
                                </tr>
                                <tr class="inbox-table-tbody-tr inbox-table-even">
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">10019</a></td>
                                    <td class="inbox-table-tbody-td">45322111</td>
                                    <td class="inbox-table-tbody-td">Daniel</td>
                                    <td class="inbox-table-tbody-td">Acevedo Jhong</td>
                                    <td class="inbox-table-tbody-td">la Libertad</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">02.2019 a 08.2019</td>
                                    <td class="inbox-table-tbody-td">ING. ESTADÍSTICA</td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#calendario-desde" ).datepicker();
            $( "#calendario-hasta" ).datepicker();
        });
    </script>
</body>
</html>