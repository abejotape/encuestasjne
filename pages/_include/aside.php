<aside class="sidebar" style="background-image: url(../../images/login/illustration.svg);">	
    <ul class="cleaner sidebar-ul">
        <li class="sidebar-li"><i class="material-icons">mail</i><span class="sidebar-li-text">Expedientes</span></li>
        <li class="sidebar-li"><i class="material-icons">drafts</i><span class="sidebar-li-text">Borradores</span></li>
        <li class="sidebar-li"><i class="material-icons">archive</i><span class="sidebar-li-text">Archivados</span></li>
    </ul>
</aside>