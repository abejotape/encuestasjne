<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="bienvenido.css">
    <title>Renovar Encuestadora Cancelada</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">            
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Renovar Encuestadora Cancelada</h1>
                    <p>Encuestadora en estado <strong>CANCELADO</strong></p>

                    <p>Es el estado ante el JNE en el registro de encuestadoras.</p>

                    <p>Se le recuerda que puede iniciar su registro en cualquier momento haciendo clic en el botón “Iniciar solicitud de registro de encuestadora”.</p>

                  
                    
                    <div class="cleaner t-a-c">
                        <button class="button button-primary button--is-large"><i class="material-icons">arrow_forward</i>Iniciar solicitud</button>
                    </div>

                    

                    <hr>



                    <div class="message-inline message-inline--is-info cleaner">
                        <i class="message-inline-icon material-icons">info</i> 
                        <p class="message-inline-text"> Cuando se notique la resolución de aceptación o cancelación se hará el bloqueo de las funcionalidades.</p>
                    </div>



                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>