<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="bienvenido.css">
    <title>Renovar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">            
                <div class="inbox-body boxcard">
                    <div class="inbox-header">
                        <div class="layout-title">Renovar Encuestadora</div>
                    </div>   
                    <p>Usted está a punto de <strong>RENOVAR</strong> su registro ante el Jurado Nacional de Elecciones!!!</p>
                    <p>Se le recuerda que sin no asegura en el xxxxxxxxx xxxxxx xxxxxx no podrá <strong>xyxyxyxyxyxyxyxyxx</strong>. Si bien los artículos de higiene y cuidado personal tienen usos bien denidos y sus procesos de producción parecieran marchar sin grandes cambios, cuatro macro tendencias vienen transformando dicha industria en el Perú y el mundo</p>
                    <p>¿Está de acuerdo con proceder con la cancelación?:</p>
                    <p>¿Está usted de acuerdo con iniciar el proceso de <span>RENOVACIÓN</span> del registro de encuestadoras?</p>

                    <div class="p-r m-b-20">
                        <label for="checkbox-test" class="form-checkbox">
                            <input type="checkbox" class="form-checkbox-input" name="check-ok" id="check-ok">
                            <label for="check-ok" class="form-radio-label">Si, estoy de acuerdo</label>
                        </label>
                    </div>
                    
                    <div class="cleaner t-a-c">
                        <button class="button button-primary"><i class="material-icons">arrow_forward</i>Siguiente</button>
                    </div>                    

                    <hr>

                    <div class="message-inline message-inline--is-info cleaner">
                        <i class="message-inline-icon material-icons">info</i> 
                        <p class="message-inline-text"> Cuando se notique la resolución de aceptación o cancelación se hará el bloqueo de las funcionalidades.</p>
                    </div>
                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>