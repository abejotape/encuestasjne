<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="../../pages/profile/resumen.css">
    <title>Renovar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Renovar Encuestadora</h1>
                    <p>Su expediente de <strong>RENOVACIÓN</strong> del registro de encuestadora fue enviado satisfactoriamente con la siguiente información, la misma que puede hacer seguimiento por este mismo medio.</p>
                    
                   <p><strong>Nro. de Expediente: ERM.000292018</strong></p>

                   <ul class="boxcard-subtitle">
                        <li>Fecha de envío: 09/10/2019</li>
                        <li>Hora de envío: 13.45 horas</li>
                    </ul>

                    
                    <div class="cleaner t-a-c m-t-40">
                        <button class="button button-primary"><i class="material-icons">arrow_forward</i>Aceptar</button>
                    </div>




                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>