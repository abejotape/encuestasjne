<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="../../pages/encuestadora/perfil.css">
    <title>Renovar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Renovar Encuestadora</h1>
                    <p>Para continuar con la renovación del registro, debe revisar la información que se presenta a continuación. Usted puede <strong>actualizar cualquier información</strong> que crea conveniente. Luego de ello grabe y envíe la información</p>
                    
                    

                    <div class="box-content m-t-30">
                        <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Datos Generales <i data-edit="datos-generales" class="material-icons resumen-edit">edit</i></div>
                            <div class="cleaner resumen-detail-content">
                                <div class="row-fluid">
                                    <div class="col-md-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Departamento: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Provincia: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Distrito: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-12 cleaner">
                                        <div class="cleaner resumen-subtitle">Domicilio: <span class="resumen-subtitle--is-detail">Mz S Lote 1 Calle las Moras Urb Ceres Ate</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box-content m-t-30">
                            <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Datos de Contacto <i data-edit="datos-contacto" class="material-icons resumen-edit">edit</i></div>
                            <div class="cleaner resumen-detail-content">
                                <div class="row-fluid">
                                    <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Teléfono: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Celular: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">P. Web: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Correo: <span class="resumen-subtitle--is-detail">Lima</span></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>


                        <div class="box-content m-t-30">
                            <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Personas <i data-edit="datos-personas" class="material-icons resumen-edit">edit</i></div>
                            <div class="cleaner resumen-detail-content">
                                <div class="row-fluid">
                                    <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Nombres: <span class="resumen-subtitle--is-detail">Moises</span></div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Apellidos: <span class="resumen-subtitle--is-detail">Francisco</span></div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">DNI: <span class="resumen-subtitle--is-detail">44901960</span></div>
                                    </div>            
                                </div>
                            </div>
                        </div>

                        <div class="box-content m-t-30">
                            <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Comprobante <i data-edit="datos-personas" class="material-icons resumen-edit">edit</i></div>
                            <div class="cleaner resumen-detail-content">
                                <div class="row-fluid">
                                    <div class="col-md-6 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Fecha: <span class="resumen-subtitle--is-detail">Moises</span></div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 cleaner m-b-20">
                                        <div class="cleaner resumen-subtitle">Código: <span class="resumen-subtitle--is-detail">Francisco</span></div>
                                    </div>         
                                </div>
                            </div>
                        </div>


                        
                        <div class="cleaner t-a-c m-t-40">
                            <button class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                            <button class="button button-primary"><i class="material-icons">arrow_forward</i>Siguiente</button>
                        </div>




                    </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>