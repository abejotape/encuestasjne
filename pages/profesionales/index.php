<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>GESTIONAR PROFESIONALES ESTADÍSTICOS</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside_prof.php") ?>
            <section class="layout-content ">
                <div class="titles-content">
                    <div class="layout-title">Gestionar profesionales estadísticos</div>
                </div>

                <div class="inbox cleaner">
                    <div class="inbox-header">
                        <div onclick="(function(){ fn.modal.open('wc-modal-crear-estadistico') })()" class="inbox-header-button inbox-header-button--is-send cleaner"><i class="material-icons">add</i> <p>Profesional</p></div>

                        <div class="inbox-header-button"><p>Cancelar</p></div>
                    </div>
                    <div class="inbox-body">
                        <table class="inbox-table cleaner">
                            <thead class="inbox-table-thead">
                                <tr class="inbox-table-thead-tr">
                                    <td class="inbox-table-thead-td">
                                        <label for="checkbox-test" class="form-checkbox">
                                            <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                        </label>
                                    </td>
                                    <td class="inbox-table-thead-td">N° col</td>
                                    <td class="inbox-table-thead-td">DNI</td>
                                    <td class="inbox-table-thead-td">Nombres</td>
                                    <td class="inbox-table-thead-td">Apellidos</td>
                                    <td class="inbox-table-thead-td">Sede</td>
                                    <td class="inbox-table-thead-td">Habilitado</td>
                                    <td class="inbox-table-thead-td">Ult. vigencia</td>
                                    <td class="inbox-table-thead-td">Carrera</td>
                                    <td class="inbox-table-thead-td"></td>
                                </tr>
                            </thead>
                            <tbody class="inbox-table-tbody">
                                <tr class="inbox-table-tbody-tr inbox-table-odd">
                                    <td class="inbox-table-thead-td"  style="width:90px">
                                        <div class="p-r flex ">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                            <i class=" material-icons p-l-10 color-secondary">bookmark</i>
                                            <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                        </div>

                                        
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">10019</a></td>
                                    <td class="inbox-table-tbody-td">45322111</td>
                                    <td class="inbox-table-tbody-td">Daniel</td>
                                    <td class="inbox-table-tbody-td">Acevedo Jhong</td>
                                    <td class="inbox-table-tbody-td">la Libertad</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">02.2019 a 08.2019</td>
                                    <td class="inbox-table-tbody-td">ING. ESTADÍSTICA</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()">
                                            <li class="table-options-li">Actualizar Datos</li>
                                            <li class="table-options-li">Agregar vigencia </li>
                                            <li class="table-options-li">Agregar N° colegiatura </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="inbox-table-details inbox-table-odd">
                                    <td class="inbox-table-details-td" colspan="10">
                                        <table class="table-details">
                                            <thead>
                                                <tr class="table-details-thead-tr">
                                                    <td class="table-details-thead-td">1</td>
                                                    <td class="table-details-thead-td">2</td>
                                                    <td class="table-details-thead-td">3</td>
                                                    <td class="table-details-thead-td">4</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="table-details-tbody-tr">
                                                    <td class="table-details-tbody-td">1</td>
                                                    <td class="table-details-tbody-td">2</td>
                                                    <td class="table-details-tbody-td">3</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>


                                <tr class="inbox-table-tbody-tr inbox-table-even">
                                    <td class="inbox-table-thead-td"  style="width:90px">
                                        <div class="p-r flex ">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                            <i class=" material-icons p-l-10 color-secondary">bookmark</i>
                                            <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                        </div>

                                        
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">10019</a></td>
                                    <td class="inbox-table-tbody-td">45322111</td>
                                    <td class="inbox-table-tbody-td">Daniel</td>
                                    <td class="inbox-table-tbody-td">Acevedo Jhong</td>
                                    <td class="inbox-table-tbody-td">la Libertad</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">02.2019 a 08.2019</td>
                                    <td class="inbox-table-tbody-td">ING. ESTADÍSTICA</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()">
                                            <li class="table-options-li">Actualizar Datos</li>
                                            <li class="table-options-li">Agregar vigencia </li>
                                            <li class="table-options-li">Agregar N° colegiatura </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr class="inbox-table-details inbox-table-even">
                                    <td class="inbox-table-details-td" colspan="10">
                                        <table class="table-details">
                                            <thead>
                                                <tr class="table-details-thead-tr">
                                                    <td class="table-details-thead-td">1</td>
                                                    <td class="table-details-thead-td">2</td>
                                                    <td class="table-details-thead-td">3</td>
                                                    <td class="table-details-thead-td">4</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="table-details-tbody-tr">
                                                    <td class="table-details-tbody-td">1</td>
                                                    <td class="table-details-tbody-td">2</td>
                                                    <td class="table-details-tbody-td">3</td>
                                                    <td class="table-details-tbody-td">4</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </section>
    </main>


    <!-- Modal Crear documento -->
    <wc-modal-crear-estadistico class="modal-content modal-content-profesional cleaner" >
        <form action="" class="formulario cleaner">
            <a onclick="(function(){ fn.modal.close('wc-modal-crear-estadistico') })()" class="modal-content--is-close cleaner" href="javascript:void(0)"><i class="material-icons modal-content-icon--is-close">cancel</i></a>
    
            <div class="modal-head cleaner">
                <strong class="modal-head-title">Agregar profesional estadístico</strong>
            </div>
            <div class="modal-body">
                <div class="titles-content">
                    <strong class="modal-head-subtitle ">Nro. Colegiatura</strong>
                    <div class="profesionals-input">
                      <div class="form-group-input cleaner d-i-b">
                            <input disabled readonly type="text" class="form-input" name="User" id="User" value="01039393">
                        </div>
                    </div>                    
                </div>  
                <div class="row-fluid cleaner">
                    <div class="col-sm-3 cleaner">
                        <label for="User" class="form-label">DNI:</label>
                        <div class="form-group-input cleaner">
                            <input type="text" class="form-input" name="User" id="User">
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Nombres:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Apellidos:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Sede:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Seleccione--</option>
                                    <option value="1">Lima</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">                
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">vigencia, desde</label>
                            <div class="form-calendars">
                                <i class="form-calendars--is-icon material-icons">calendar_today</i>
                                <input type="text" class="form-input" id="calendario-desde" placeholder="dd-mm--yyy">
                            </div>
                        </div>
                    </div>    
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Vigencia, hasta</label>
                            <div class="form-calendars">
                                <i class="form-calendars--is-icon material-icons">calendar_today</i>
                                <input type="text" class="form-input" id="calendario-desde" placeholder="dd-mm--yyy">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">celular</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Carrera</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Seleccione--</option>
                                    <option value="1">Derecho</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">
                  <div class="col-sm-3 cleaner">
                      <div class="form-group cleaner m-b-20">
                          <label for="User" class="form-label">Departamento</label>
                          <label class="form-group-select " for="departamento">
                              <select id="departamento" name="options">
                                  <option value="">--Seleccione el departamento--</option>
                                  <option value="1">Lima</option>
                              </select>
                          </label>
                      </div>
                  </div>
                    
                  <div class="col-sm-3 cleaner">
                      <div class="form-group cleaner m-b-20">
                          <label for="User" class="form-label">Provincia</label>
                          <label class="form-group-select " for="departamento">
                              <select id="departamento" name="options">
                                  <option value="">--Seleccione la provincia--</option>
                                  <option value="1">Lima</option>
                              </select>
                          </label>
                      </div>
                  </div>
                  <div class="col-sm-3 cleaner">
                      <div class="form-group cleaner m-b-20">
                          <label for="User" class="form-label">Distrito</label>
                          <label class="form-group-select " for="departamento">
                              <select id="departamento" name="options">
                                  <option value="">--Seleccione el distrito--</option>
                                  <option value="1">Lima</option>
                              </select>
                          </label>
                      </div>
                  </div>
                  <div class="col-sm-3 cleaner">
                      <div class="form-group cleaner m-b-20">
                          <label for="User" class="form-label">Universidad</label>
                          <label class="form-group-select " for="departamento">
                              <select id="departamento" name="options">
                                  <option value="">--Seleccione--</option>
                                  <option value="1">San Marcos</option>
                              </select>
                          </label>
                      </div>
                  </div>
                </div>
                <div class="row-fluid cleaner">            
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Dirección:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Habilitado:</label>
                            <label for="checkbox-test" class="form-checkbox">
                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                <label for="checkbox-test" class="form-checkbox-label">Si</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">            
                    <div class="col-md-12 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Observaciones:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-content cleaner">
                <button onclick="(function(){ fn.modal.close('wc-modal-crear-estadistico') })()" type="button" class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                <button type="button" class="button button-primary" ><i class="material-icons">highlight_off</i>Guardar</button>
            </div>
        </form>
    </wc-modal-crear-estadistico>


    <?php include '../_include/footer.php' ?>
    
</body>
</html>