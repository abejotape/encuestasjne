class wcRegister extends HTMLElement{

    constructor(){
        super()
        this.importDocument = document.currentScript.ownerDocument;       
        let _shadowRoot = this.attachShadow({mode : "open"})
        const _template = this.importDocument.querySelector("#tmpRegister")
        _shadowRoot.appendChild(_template.content.cloneNode(true));
    } 

    attributeChangedCallback (name, oldValue, newValue) {
        this.shadowRoot.querySelector(".title").textContent = newValue;
    }
    
    connectedCallback(){       
        // const ValidarDNI = this.shadowRoot.querySelector("#ValidarDNI");
        // ValidarDNI.addEventListener("click", e => {
        //     alert("ValidarDNI")
        // }); 

         

        const ModalCrearCuenta = this.shadowRoot.querySelector("#ModalCrearCuenta");  

        if(this.shadowRoot.querySelectorAll('.button-modal-close') != null){        
            let btnClose = this.shadowRoot.querySelectorAll('.button-modal-close');
            btnClose.forEach(function(el) {
                el.addEventListener('click', function(){
                    ModalCrearCuenta.classList.remove('modal-content--is-open')
                    document.getElementsByTagName('body')[0].classList.remove('modal--is-open')
                });
            });
        
        }
        

    }	




 
    get title() {
        return this.getAttribute('title');
    }

    set title(value) {
        this.setAttribute('title', value);
    }

    static get observedAttributes () {
        return ['title'];
    }     

    
}

customElements.define("jne-register", wcRegister )