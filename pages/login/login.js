"use strict";

// Detect mobile
window.onload = function() {
    document.getElementsByClassName('icon-menu').innerHTML = "menu"
    document.getElementById('main').classList.remove('mais--is-collapsed')

    if(window.innerWidth < 768){
        document.getElementById('main').classList.add('mobile')
    }else{
        document.getElementById('main').classList.remove('mobile')
    }
}
// Resize
window.onresize = function(ev) {
    document.getElementsByClassName('icon-menu').innerHTML = "menu"    
    document.getElementById('main').classList.remove('mais--is-collapsed')
    
    if(ev.target.innerWidth < 768){
        document.getElementById('main').classList.add('mobile')
    }else{
        document.getElementById('main').classList.remove('mobile')
    }
};


// Poner el asterístico para todo los importantes
if(document.querySelectorAll('[required]') != null) {
    let required = document.querySelectorAll('[required]');
    required.forEach(function(el) {
        el.innerHTML = el.textContent + '<i class="required">*</i>'
    })
}


// Funciones de Login JS

let btnVolverLogin = document.querySelector('#volverLogin');
btnVolverLogin.addEventListener('click', function(){
    document.getElementsByTagName('body')[0].classList.remove('is--register')
    document.getElementById("mainRegister").removeAttribute("style")
    document.getElementById("mainLogin").removeAttribute("style")
});


let btnCrearCuenta = document.querySelector('#CrearCuenta');
btnCrearCuenta.addEventListener('click', function(){
    document.getElementsByTagName('body')[0].classList.add('is--register')
    setTimeout(function(){
        document.getElementById("mainLogin").style.display = "none";
        document.getElementById("mainRegister").style.display = "block";

    },450)
});

// let btnCrearCuenta = document.querySelector('#CrearCuenta');
// btnCrearCuenta.addEventListener('click', function(){
//     document.getElementsByTagName('body')[0].classList.add('modal--is-open')
//     let wcomponent = document.querySelector('jne-register' )
//     let child = wcomponent.shadowRoot.querySelector('#ModalCrearCuenta')
//     child.classList.add('modal-content--is-open')
// });

let btnRecuperarCuenta = document.querySelector('#RecuperarCuenta');
btnRecuperarCuenta.addEventListener('click', function(){
    document.getElementsByTagName('body')[0].classList.add('modal--is-open')
    let wcomponent = document.querySelector('jne-recovery' )
    let child = wcomponent.shadowRoot.querySelector('#ModalRecuperarCuenta')
    child.classList.add('modal-content--is-open')
});



// Efectos de animación
var textWrapper = document.querySelector('.login-tutorial');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({loop: false})
.add({
    targets: '.login-tutorial .letter',
    scale: [4,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 950,
    delay: function(el, i) {
        return 70*i;
    }
}).add({
    targets: '.ml2',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
});


var textWrapper = document.querySelector('.navegadores-texto');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({loop: true})
  .add({
    targets: '.navegadores-texto .letter',
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 900,
    delay: function(el, i) {
      return 500 + 30 * i;
    }
  }).add({
    targets: '.navegadores-texto .letter',
    translateX: [0,-30],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1100,
    delay: function(el, i) {
        return 100 + 30 * i;
    }
});


