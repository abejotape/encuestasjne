<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>Cancelar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Cancelar registro</h1>
                    <p>Su expediente de <strong>CANCELACIÓN</strong> del registro de encuestadora fue enviado satisfactoriamente con la siguiente información, la misma que puede hacer seguimiento por este mismo medio</p>
                    <p>Nro. de Expediente: <strong>ERM.000292018</strong></p>
                    <ul class="boxcard-subtitle">
                        <li>Fehca: 09/10/2019</li>
                        <li>Hora: 13.45 horas</li>
                    </ul>
                    
                    <div class="cleaner t-a-c m-t-40">
                        <button class="button button-primary"><i class="material-icons">send</i>Aceptar</button>
                    </div>




                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>