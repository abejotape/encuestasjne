<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>Cancelar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Cancelar registro</h1>
                    <p>La encuestadora tiene aún documentos pendientes que presentar con los siguientes expedientes:</p>
                    <ul class="boxcard-subtitle">
                        <li>ERM.0002922018, subsanar informe.</li>
                        <li>ERM.0002922018, subsanar informe.</li>
                    </ul>
                    
                    

                    <p>¿Desea continuar con el proceso de cancelación?</p>

                    
                    <div class="cleaner t-a-c m-t-40">
                        <button class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                        <button class="button button-primary"><i class="material-icons">arrow_forward</i>Siguiente</button>
                    </div>




                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>