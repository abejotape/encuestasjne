<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>Cancelar Encuestadora</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Cancelar registro</h1>
                    <p>El registro de la encuestadoras <strong> “XYZ SAC”</strong> ha sido cancelado!!</p>

                    <p>Se le recuerda que en cualquier momento puede volver a solicitar su registro a través d este mismo sistema de información y con sus
mismas credenciales de acceso (usaurio y clave)</p>

                    
                    <div class="cleaner t-a-c m-t-40">
                        <button class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                        <button class="button button-primary"><i class="material-icons">send</i>Finalizar</button>
                    </div>




                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>