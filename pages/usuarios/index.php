<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>GESTIONAR USUARIOS</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside_prof.php") ?>
            <section class="layout-content">                
                <div class="inbox cleaner"> 
                    <div class="inbox-header">
                        <div class="layout-title">Gestionar usuarios</div>
                        <div onclick="(function(){ fn.modal.open('wc-modal-creardocumento') })()" class="inbox-header-button inbox-header-button--is-send cleaner"><i class="material-icons">add</i><p>usuario</p></div>
                        <div class="inbox-header-button"><p>Cancelar</p></div>
                    </div>
                    <div class="inbox-body">
                        <table class="inbox-table cleaner">
                            <thead class="inbox-table-thead">
                                <tr class="inbox-table-thead-tr">
                                    <td class="inbox-table-thead-td">
                                        <label for="checkbox-test" class="form-checkbox">
                                            <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>                                            
                                        </label>

                                    </td>
                                    <td class="inbox-table-thead-td">DNI</td>
                                    <td class="inbox-table-thead-td">Nombres</td>
                                    <td class="inbox-table-thead-td">Apellidos</td>
                                    <td class="inbox-table-thead-td">Sede</td>
                                    <td class="inbox-table-thead-td">Celular</td>
                                    <td class="inbox-table-thead-td">Usuario</td>
                                    <td class="inbox-table-thead-td">E-mail</td>
                                    <td class="inbox-table-thead-td">Habilitado</td>
                                    <td class="inbox-table-thead-td">Tipo Usuario</td>
                                    <td class="inbox-table-thead-td">...</td>
                                </tr>
                            </thead>
                            <tbody class="inbox-table-tbody">

                                <!-- Fila con su detalle -->
                                <tr id="test" class="inbox-table-tbody-tr inbox-table-odd">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>                                            
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">45098762</a></td>
                                    <td class="inbox-table-tbody-td">Daniel</td>
                                    <td class="inbox-table-tbody-td">Acebedo Jhong</td>
                                    <td class="inbox-table-tbody-td">La Libertad</td>
                                    <td class="inbox-table-tbody-td">98976543</td>
                                    <td class="inbox-table-tbody-td">Dacevedo</td>
                                    <td class="inbox-table-tbody-td">Dacevedo@hotmail.com</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">Administrador</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="hideTableOptions(event)">
                                            <li class="table-options-li">Actualizar datos</li>
                                            <li class="table-options-li">Eliminar</li>
                                        </ul>
                                    </td>
                                </tr>
                                 
                                <!-- end Fila con su detalle -->

                                <tr class="inbox-table-tbody-tr inbox-table-even">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="radio" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">40332212</a></td>
                                    <td class="inbox-table-tbody-td">Miguel Vicente</td>
                                    <td class="inbox-table-tbody-td">Agurto Rondoy</td>
                                    <td class="inbox-table-tbody-td">Ayacucho</td>
                                    <td class="inbox-table-tbody-td">98765432</td>
                                    <td class="inbox-table-tbody-td">Magurto</td>
                                    <td class="inbox-table-tbody-td">Magurto@hotmail.com</td>
                                    <td class="inbox-table-tbody-td">No</td>
                                    <td class="inbox-table-tbody-td">Registrador</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="hideTableOptions(event)">
                                            <li class="table-options-li">Actualizar datos</li>
                                            <li class="table-options-li">Eliminar</li>
                                        </ul>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </section>
    </main>

    <!-- Modal Crear documento -->
    <wc-modal-creardocumento class="modal-content cleaner" >
        <form action="" class="formulario cleaner">
              <a onclick="(function(){ fn.modal.close('wc-modal-creardocumento') })()" class="modal-content--is-close cleaner" href="javascript:void(0)"><i class="material-icons modal-content-icon--is-close">cancel</i></a>
      
              <div class="modal-head cleaner">
                  <strong class="modal-head-title">Agregar Usuario</strong>
              </div>
              <div class="modal-body">
                <div class="row-fluid cleaner">
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">DNI:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Nombres:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Apellidos:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Sede:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Seleccione--</option>
                                    <option value="1">Lima</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">   
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">celular</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">E-mail:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>    
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Carrera</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Seleccione--</option>
                                    <option value="1">Derecho</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Habilitado:</label>
                            
                            <label for="checkbox-test" class="form-checkbox">
                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test"  >
                                <label for="radio-test" class="form-checkbox-label"> Si</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">                  
                    <div class="col-md-12 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Observaciones:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>           
    
                </div>
            </div>
            <div class="button-content cleaner">
                <button onclick="(function(){ fn.modal.close('wc-modal-creardocumento') })()" type="button" class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                <button type="button" class="button button-primary" ><i class="material-icons">highlight_off</i>Guardar</button>
            </div>
        </form>
    </wc-modal-creardocumento>

    <?php include '../_include/footer.php' ?>
</body>
</html>