<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>RESUMEN</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                    <div class="inbox-body boxcard">
                        <h1 class="boxcard-title">Aviso de Confirmación</h1>

                        <p>Gracias por actualizar tus datos!!!</p>
                        <p>En la transacción se han realizado los siguientes cambios:</p>
                        <p>Dirección: Av. Nicolás de Piérola #120</p>
                        <hr>

                        <div class="message-inline message-inline--is-info cleaner">
                            <i class="message-inline-icon material-icons">info</i> 
                            <p class="message-inline-text">Esta información será considerada formalmente a par tir de hoy 13/08/2019 a las 13:30 horas</p>
                        </div>  
                                                                   
                    </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>