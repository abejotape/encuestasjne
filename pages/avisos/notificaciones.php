<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>NOTIFICACIONES</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                    <div class="inbox-body boxcard">
                        <h1 class="boxcard-title">Lista de Notificaciones</h1>
                        <hr>

                        <div class="boxcard-subtitle">
                            <p>La Dirección Central de Gestión Institucional del JNE ha generado un aviso de BIENVENIDA...</p>
                            <p><span><i class="color-secondary m-r-10 material-icons">mail</i></span>12/09/2019</p>
                        </div>

                        <div class="boxcard-subtitle">
                            <p>La Dirección Central de Gestión Institucional del JNE ha generado un aviso de BIENVENIDA...</p>
                            <p><span><i class="color-secondary m-r-10 material-icons">mail</i></span>12/09/2019</p>
                        </div>

                        <div class="boxcard-subtitle">
                            <p>La Dirección Central de Gestión Institucional del JNE ha generado un aviso de BIENVENIDA...</p>
                            <p><span><i class="color-secondary m-r-10 material-icons">mail</i></span>12/09/2019</p>
                        </div>

                        <div class="message-inline message-inline--is-info cleaner">
                            <i class="message-inline-icon material-icons">info</i> 
                            <p class="message-inline-text">Esta información ha sido generada el día 13/08/2019 a las 13:30 horas</p>
                        </div>  
                                                                   
                    </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>