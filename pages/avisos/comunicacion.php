<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>COMUNICACIÓ </title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                    <div class="inbox-body boxcard">
                        <h1 class="boxcard-title">Aviso de comunicación</h1>
                        <p>El JNE hace de conocimiento!!!</p>
                        <p>Se ha aprobado el registro de su encuestadora mediante resolución Resolución Administrativa Nro. 234 - 2019 - JNE. También se le ha hecho llegar el certificado de vigencia.</p>                        
                        <p>Se le sugiere revisar su casilla electrónica</p>
                        <hr>
                        
                        <div class="message-inline message-inline--is-info cleaner">
                            <i class="message-inline-icon material-icons">info</i> 
                            <p class="message-inline-text">Esta información ha sido generada el día 13/08/2019 a las 13:30 horas</p>
                        </div>                                            
                    </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>