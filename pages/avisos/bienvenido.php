<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="bienvenido.css">
    <title>BIENVENIDO</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                <div class="inbox-body boxcard">
                    <h1 class="boxcard-title">Bienvenido(a) a <strong>encuestadoras.com</strong>  del JNE</h1>
                    <p>Se creó exitosamente su usuario. En base a la normativa... Si bien los ar tículos de higiene y cuidado personal tienen usos bien definidos y sus procesos de producción parecieran marchar sin grandes cambios, cuatro macro tendencias vienen transformando dicha industria en el Perú y el mundo.</p>
                    <p>Los detalles de tu cuenta son:</p>
                    <div class="boxcard-subtitle">
                        <p>Nombre: <span>10404059504</span></p>
                        <p>Clave: <span>129828</span></p>
                    </div>
                    
                    <hr>
                    

                    <div class="message-inline message-inline--is-info cleaner">
                        <i class="message-inline-icon material-icons">info</i> 
                        <p class="message-inline-text">La primera vez que te logueas debes cambiar tu contraseña</p>
                    </div>



                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>