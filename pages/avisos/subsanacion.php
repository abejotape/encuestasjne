<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>SUBSANACION</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">                
                    <div class="inbox-body boxcard">
                        <h1 class="boxcard-title">Aviso de Subsanación</h1>
                        
                        <p>El JNE hace de conocimiento!!!</p>
                        <p>El informe per teneciente al expediente <span>ERM.2018010945</span> tiene observaciones.</p>
                        <p>Se le sugiere revisar su casilla electrónica</p>
                        <hr>

                        <div class="message-inline message-inline--is-info cleaner">
                            <i class="message-inline-icon material-icons">info</i> 
                            <p class="message-inline-text">Esta información ha sido generada el día 13/08/2019 a las 13:30 horas</p>
                        </div>  
                                                                   
                    </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>