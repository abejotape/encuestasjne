<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="archivos.css">
    <title>ARCHIVOS</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside_prof.php") ?>
            <section class="layout-content">
                <div class="titles-content">
                    <div class="layout-title">Archivos</div>
                </div>                             
                
                <div class="inbox cleaner archivos-content">
                    <div class="inbox-header--is-filter">
                        <div class="cleaner">
                            <label  class="form-label archivos-div-label">Archivo a subir</label>
                            <label class="form-group-select archivos-div-label">
                                <select>
                                    <option value="">--Seleccione--</option>
                                    <option value="1">1</option>
                                </select>
                            </label>
                        </div>
                        <div class="cleaner m-l-10">
                            <label class="form-label archivos-div-label archivos-div-label-titulo">Titulo o descripción</label>
                            <div class="cleaner">
                                <div class="">
                                    <input type="text" class="form-input archivos-div-input">
                                </div>
                            </div>                            
                        </div>                
                    </div>     
                    
                    <div class="archivos-subir-texto">  
                        <p class="archivos-subir-texto-p1">Base de datos</p>
                        <div class="archivos-subir-texto-div">
                            <i class="material-icons">cancel</i>
                            <p class="archivos-subir-texto-p2"> Formatos Permitidos: XLS, XLSX o SAV</p> 
                        </div>                         
                    </div> 
                    <div class="archivos-subir-cuadros"> 
                        <div class="archivos-subir-arrastre">
                            <p class="archivos-subir-arrastre-p1"><span>Arrastre aqui</span>, el archivo <span>para cargar</span></p>
                            <p class="archivos-subir-arrastre-p2">Microsoft Excel o SAV (Maximo 10 Mb)</p>
                        </div>      
                        <div class="archivos-mostrar">
                            <div class="archivos-mostrar-div1">
                                <p>XLSX</p>
                            </div>
                            <div class="archivos-mostrar-div2">
                                <div class="archivos-mostrar-text">
                                    <p class="archivos-mostrar-div2-p1">bd_Informe</p>
                                    <p class="archivos-mostrar-div2-p2">24Mb</p> 
                                </div>
                                <div class="archivos-mostrar-icon">
                                    <a href="#"><i class="material-icons archivos-icons-delete">delete</i></a> 
                                </div>                                                               
                            </div>                              
                        </div>      
                        <div class="inbox-header-button archivos-agregar">
                                <i class="material-icons">send</i><p>Agregar</p>
                        </div>                                                                   
                    </div>    
                    
                    <div class="inbox-body">
                        <table class="inbox-table cleaner archivos-table">                          
                            <tbody class="inbox-table-tbody">
                              <tr class="inbox-table-tbody-tr inbox-table-even">
                                  <td class="inbox-table-tbody-td" style="width:70px">
                                      <div class="p-r">                                          
                                          <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                      </div>
                                  </td>
                                  <td class="inbox-table-tbody-td"><a href="#" class="link">Informe de la Encuesta</a></td>
                                  <td class="inbox-table-tbody-td">Registro de encuestadora</td>
                                  <td class="inbox-table-tbody-td">21/01/2019</td>
                                  <td class="inbox-table-tbody-td">3.0 Mb</td>                                  
                                  <td class="inbox-table-tbody-td">  <a href="#"><i class="material-icons archivos-icons-delete">delete</i></a> </td>
                                  
                              </tr>
                              <tr class="inbox-table-tbody-tr inbox-table-even">
                                  <td class="inbox-table-tbody-td" style="width:70px">
                                      <div class="p-r">
                                          <i class="inbox-table-dropdown material-icons">arrow_drop_down_circle</i>
                                      </div>
                                  </td>
                                  <td class="inbox-table-tbody-td"><a href="#" class="link">BD informe de encuesta</a></td>
                                  <td class="inbox-table-tbody-td">BD informe de encuesta</td>
                                  <td class="inbox-table-tbody-td">21/01/2019</td>
                                  <td class="inbox-table-tbody-td">2.4 Mb</td>                                  
                                  <td class="inbox-table-tbody-td">  <a href="#"><i class="material-icons archivos-icons-delete">delete</i></a></td>
                              
                              </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="inbox-header">
                        
                        <div class="inbox-header-button archivos-button-cancelar"><p>Cancelar</p></div>
                        <div class="inbox-header-button archivos-button-siguiente">
                                <i class="material-icons">send</i><p>Siguiente</p>
                        </div>                         
                    </div>
                </div>


            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>
</body>
</html>