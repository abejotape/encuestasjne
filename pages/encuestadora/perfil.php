<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="perfil.css">
    <title>PERFIL DE ENCUESTADORA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">PERFIL de Encuestadora</h1>
                </div>

                <div class="box-content box-content-center">
                    <i data-edit="datos-generales" class="material-icons resumen-edit">edit</i>
                    <div class="resumen-content">
                        <div class="resumen-foto">
                            <div class="perfil-foto resumen-foto--is-img" style="background-image: url(../../images/main/ipsos.jpg)"> </div>
                        </div>
                        <div class="resumen-datos cleaner">
                            <div class="cleaner resumen-title">Ipsos Opinion y Mercado S.A.</div>
                            <div class="cleaner resumen-subtitle">20260497414</div>
                        </div>
                    </div>                  
                </div>

                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Datos Generales <i data-edit="datos-generales" class="material-icons resumen-edit">edit</i></div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Departamento: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Provincia: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Distrito: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-12 cleaner">
                                <div class="cleaner resumen-subtitle">Domicilio: <span class="resumen-subtitle--is-detail">Mz S Lote 1 Calle las Moras Urb Ceres Ate</span></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Datos de Contacto <i data-edit="datos-contacto" class="material-icons resumen-edit">edit</i></div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Teléfono: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Celular: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">P. Web: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Correo: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            
                        </div>
                    </div>
                </div>


                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Personas <i data-edit="datos-personas" class="material-icons resumen-edit">edit</i></div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Nombres: <span class="resumen-subtitle--is-detail">Moises</span></div>
                            </div>
                            <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Apellidos: <span class="resumen-subtitle--is-detail">Francisco</span></div>
                            </div>
                            <div class="col-md-4 col-sm-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">DNI: <span class="resumen-subtitle--is-detail">44901960</span></div>
                            </div>            
                        </div>
                    </div>
                </div>

                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Comprobante <i data-edit="datos-personas" class="material-icons resumen-edit">edit</i></div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-6 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Fecha: <span class="resumen-subtitle--is-detail">Moises</span></div>
                            </div>
                            <div class="col-md-6 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Código: <span class="resumen-subtitle--is-detail">Francisco</span></div>
                            </div>         
                        </div>
                    </div>
                </div>


            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    
</body>
</html>