class Profile{
	constructor(){
		
		window.buttons = document.getElementsByClassName('tabs-button');
		for(let btn of buttons){
			btn.addEventListener('click', this.showContentTab, false)
		}
	}
	showContentTab(e){
		for(let btn of window.buttons){
			btn.classList.remove("tabs-button--is-active")
		}
		e.target.classList.add('tabs-button--is-active')
		let tab = e.target.getAttribute('data-tab');
		var contenido = {
			"1" : function(){
				document.getElementById('tab-conten-1').style.display = null
				document.getElementById('tab-conten-2').style.display = "none"
				document.getElementById('tab-conten-3').style.display = "none"
				document.getElementById('tab-conten-4').style.display = "none"
			},
			"2" : function(){
				document.getElementById('tab-conten-1').style.display = "none"
				document.getElementById('tab-conten-2').style.display = null
				document.getElementById('tab-conten-3').style.display = "none"
				document.getElementById('tab-conten-4').style.display = "none"
			},
			"3" : function(){
				document.getElementById('tab-conten-1').style.display = "none"
				document.getElementById('tab-conten-2').style.display = "none"
				document.getElementById('tab-conten-3').style.display = null
				document.getElementById('tab-conten-4').style.display = "none"
			},
			"4" : function(){
				document.getElementById('tab-conten-1').style.display = "none"
				document.getElementById('tab-conten-2').style.display = "none"
				document.getElementById('tab-conten-3').style.display = "none"
				document.getElementById('tab-conten-4').style.display = null
			}
		}

		if(tab != null)
			contenido[tab]()
	}
	
}

new Profile()