<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="verificar-medio.css">
    <title>VERIFICAR CORREO</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                <div class="titles-content">
                    <h1 class="title-primary">Verificar Correo</h1>
                </div>
                <div class="box-content">

                    <div class="confirmar-medio">
                        <div class="confirmar-medio-icon">
                            <img src="../../images/icons/mail.svg" alt="Correo" class="w-100">
                        </div>
                        <div class="confirmar-medio-mensaje">
                            <p>Hemos enviaremos un código de vericación a <span class="color-secondary">hu*****@gmail.com</span></p>
                            <p class="font-lucida">Escríbalo a continuación para comprobar que esta sea su dirección de correo electrónico.</p>
                            <div class="confirmar-form">
                                <div class="form-group cleaner m-b-20">
                                    <label for="User" class="form-label">Correo Electrónico:</label>
                                    <div class="form-group-input cleaner">
                                        <input type="text" class="form-input" name="User" id="User">
                                    </div>                                    
                                </div>
                                <button type="button" class="button button-primary">Enviar <i class="form-button-icon icon-sent"></i></button>
                            </div>
                        </div>

                    </div>
                    
                </div>

            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    
</body>
</html>