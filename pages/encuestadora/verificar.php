<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="verificar.css">
    <title>VERIFICAR ENCUESTADORA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                <div class="titles-content">
                    <h1 class="title-primary">Verificar Encuestadora</h1>
                </div>
                <div class="box-content">
                    <div class="confirmar">
                        <div class="confirmar-opcion">
                            <div class="confirmar-opcion-icon">
                                <img src="../../images/icons/mail.svg" alt="Correo">
                            </div>
                            <div class="confirmar-opcion-text">
                                <p>Enviar correo a:</p>
                                <p>bg..@gmail.com</p>
                                <button type="button" class="button button-secondary m-t-20">Enviar <i class="form-button-icon icon-sent"></i></button>
                            </div>  
                        </div>

                        <div class="confirmar-opcion">
                            <div class="confirmar-opcion-icon">
                                <img src="../../images/icons/sms.svg" alt="Correo">
                            </div>
                            <div class="confirmar-opcion-text">
                                <p>Enviar mensaje de texto a:</p>
                                <p>***9592</p>
                                <button type="button" class="button button-primary m-t-20">Enviar <i class="form-button-icon icon-sent"></i></button>
                            </div>  
                        </div>
                    </div>
                </div>

            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    
</body>
</html>