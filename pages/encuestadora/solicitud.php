<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="solicitud.css">
    <title>VERIFICAR ENCUESTADORA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                <div class="titles-content">
                    <h1 class="title-primary">Solicitud de Registro Encuestadora</h1>
                </div>
                <div class="box-content">
                    <div class="solicitud">
                        <p>Su expediente de <span class="color-secondary">solicitud de REGISTRO</span> de encuestadora fue enviado satisfactoriamente con la siguiente información, la misma que puede hacer seguimiento por este mismo medio.</p>
                    </div>
                    <div class="boxcard-subtitle">
                        <p>Nro. de Expediente: <span>ERM.2018010945</span></p>
                        <p>Fecha: <span>09/10/2019</span></p>
                        <p>Hora: <span>13.45 horas</span></p>
                    </div>


                    <button type="button" class="button button-primary m-t-20">Aceptar <i class="form-button-icon icon-sent"></i></button>

                </div>

            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    
</body>
</html>