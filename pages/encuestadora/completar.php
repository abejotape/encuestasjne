<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="completar.css">
    <title>PERFIL DE ENCUESTADORA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">PERFIL de Encuestadora</h1>
                </div>
                
                
               <div class="tac chart-content">
                    <div class="chart" data-percent="73"><span>73</span>%</div>
               </div>




                <div class="steps cleaner">

                    <div class="steps-block  success ">
                        <div class="step-circle" data-number="1"></div>
                        <div class="step-content">

                            <div class="step-title">DATOS GENERALES <i class="step-slidedown"></i></div>
                            <div class="step-info">
                                <form action="" class="formulario cleaner">
                                    <div class="row-fluid cleaner">
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Razón Social:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">RUC:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div>                            
                                    
                                        <div class="col-sm-6 col-md-4 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="departamento" class="form-label">Departamento:</label>
                                                <label class="form-group-select " for="departamento">
                                                    <select id="departamento" name="options">
                                                        <option value="">--Elegir--</option>
                                                        <option value="1">Lima</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="departamento" class="form-label">Provincia:</label>
                                                <label class="form-group-select " for="departamento">
                                                    <select id="departamento" name="options">
                                                        <option value="">--Elegir--</option>
                                                        <option value="1">Lima</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="departamento" class="form-label">Distrito:</label>
                                                <label class="form-group-select " for="departamento">
                                                    <select id="departamento" name="options">
                                                        <option value="">--Elegir--</option>
                                                        <option value="1">Lima</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Domicilio:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div>
                                    </div>                        
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="steps-block error">
                        <div class="step-circle" data-number="2"></div>
                        <div class="step-content">
                            <div class="step-title">DATOS DE CONTACTO <i class="step-slidedown"></i></div>
                            <div class="step-info">
                                <form action="" class="formulario cleaner">
                                    <div class="row-fluid cleaner">
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Teléfono:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Celular:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Página Web:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-6 cleaner">
                                            <div class="form-group cleaner m-b-20">
                                                <label for="User" class="form-label">Correo:</label>
                                                <div class="form-group-input cleaner">
                                                    <input type="text" class="form-input" name="User" id="User">
                                                </div>
                                            </div>
                                        </div>                            
                                    
                                    </div>                        
                                </form>
                            </div>
                        </div>
                    </div>

                </div>


            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.easy-pie-chart/1.0.1/jquery.easy-pie-chart.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.easy-pie-chart/1.0.1/jquery.easy-pie-chart.css">
    <script>
        $(document).ready(function() {
            $('.steps-block').on('click', function(){
                $('.steps-block').removeClass('active')
                $(this).addClass('active')
            })

            $('.chart').easyPieChart({
                // The color of the curcular bar. You can pass either a css valid color string like rgb, rgba hex or string colors. But you can also pass a function that accepts the current percentage as a value to return a dynamically generated color.
                barColor: '#FFA549',
                // The color of the track for the bar, false to disable rendering.
                trackColor: '#ccc',
                // The color of the scale lines, false to disable rendering.
                scaleColor: '#dfe0e0',
                // Defines how the ending of the bar line looks like. Possible values are: butt, round and square.
                lineCap: 'round',
                // Width of the bar line in px.
                lineWidth: 10,
                // Size of the pie chart in px. It wll always be a square.
                size: 120,
                // Time in milliseconds for a eased animation of the bar growing, or false to deactivate.
                animate: 1000,
                // Callback function that is called at the start of any animation (only if animate is not false).
                onStart: $.noop,
                // Callback function that is called at the end of any animation (only if animate is not false).
                onStop: $.noop
              });                                 
        });
    </script>
    
</body>
</html>