<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="encuestadora.css">
    <link rel="stylesheet" href="perfil.css">
    <title>COMPLETAR PERFIL ENCUESTADORA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                <div class="inbox-header">
                    <div class="layout-title">Completar Perfil de Encuestadora</div>
                </div>  
                <div class="tabs-content cleaner m-t-20">
                    <div class="tabs-button cleaner tabs-button--is-active" data-tab="1">Datos Generales</div>
                    <div class="tabs-button cleaner" data-tab="2">Datos de Contacto</div>
                    <div class="tabs-button cleaner" data-tab="3">Personas</div>
                    <div class="tabs-button cleaner" data-tab="4">Comprobante</div>
                </div>
                <div class="box-content">
                    <div id="tab-conten-1" class="tabs-content cleaner">  
                        <form action="" class="formulario cleaner">
                            <div class="row-fluid cleaner">
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Razón Social:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">RUC:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>                            
                            
                                <div class="col-sm-6 col-md-4 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="departamento" class="form-label">Departamento:</label>
                                        <label class="form-group-select " for="departamento">
                                            <select id="departamento" name="options">
                                                <option value="">--Elegir--</option>
                                                <option value="1">Lima</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="departamento" class="form-label">Provincia:</label>
                                        <label class="form-group-select " for="departamento">
                                            <select id="departamento" name="options">
                                                <option value="">--Elegir--</option>
                                                <option value="1">Lima</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="departamento" class="form-label">Distrito:</label>
                                        <label class="form-group-select " for="departamento">
                                            <select id="departamento" name="options">
                                                <option value="">--Elegir--</option>
                                                <option value="1">Lima</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Domicilio:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </form>
                    </div>
                    <div id="tab-conten-2" class="tabs-content cleaner" style="display:none">  
                        <form action="" class="formulario cleaner">
                            <div class="row-fluid cleaner">
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Teléfono:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Celular:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Página Web:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Correo:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>                            
                            
                            </div>                        
                        </form>
                    </div>
                    <div id="tab-conten-3" class="tabs-content cleaner" style="display:none">  
                        
                        <div class="dni-personas cleaner">
                            <div class="form-double cleaner">
                                <div class="form-double-search cleaner">
                                    <label for="User" class="form-label">DNI representante legal:</label>
                                    <div class="form-group-input cleaner">
                                        <input type="text" class="form-input" name="User" id="User">
                                    </div>
                                </div>
                                <button class="button button-terciary" id="prueba">Validar</button>
                            </div>
                            <div class="dni-personas cleaner">
                                <table class="table cleaner">
                                    <thead class="table-thead">
                                        <tr>
                                            <td class="table-thead-td">Nombre</td>
                                            <td class="table-thead-td">Apellidos</td>
                                            <td class="table-thead-td">Dni</td>
                                            <td class="table-thead-td"></td>
                                        </tr>
                                    </thead>
                                    <tbody class="table-tbody">
                                        <tr>
                                            <td class="table-thead-td">Moises Francisco</td>
                                            <td class="table-thead-td">Yance Quispe</td>
                                            <td class="table-thead-td">44901960</td>
                                            <td class="table-thead-td t-a-r"><i class="persona-delete material-icons">highlight_off</i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                                                                            
                        
                    </div>
                    <div id="tab-conten-4" class="tabs-content cleaner" style="display:none">  
                        <form action="" class="formulario cleaner">
                            <div class="row-fluid cleaner">
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Fecha:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 cleaner">
                                    <div class="form-group cleaner m-b-20">
                                        <label for="User" class="form-label">Código:</label>
                                        <div class="form-group-input cleaner">
                                            <input type="text" class="form-input" name="User" id="User">
                                        </div>
                                    </div>
                                </div>                            
                            
                            </div>                        
                        </form>
                    </div>
                    <div class="button-content cleaner">
                        <button class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                        <button class="button button-primary" id="prueba"><i class="material-icons">highlight_off</i>Guardar</button>
                        <button class="button button-terciary"><i class="material-icons">arrow_forward</i>Siguiente</button>
                    </div>                    
                </div>
            </section>
        </section>
    </main>

    <?php include '../_include/footer.php' ?>   
    <script src="encuestadora.js"></script>
    
</body>
</html>