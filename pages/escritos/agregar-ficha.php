<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>AGREGAR FICHA TÉCNICA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">Agregar ficha de Ecuestas</h1>
                </div>


                <div class="box-content">
                    <div class="row-fluid cleaner">                        
                        <div class="col-md-6 cleaner">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Financiamiento<i class="important">*</i>:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid cleaner">                        
                        <div class="col-md-12 cleaner">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Objetivos:</label>
                                <div class="form-group-input cleaner">
                                    <textarea class="form-textarea" maxlength="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid cleaner">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Tamaño de la población:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Tamaño de la muestra:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Margen de error:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Nivel de conanza:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Representatividad:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Tipo de muestreo:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid cleaner">
                        <div class="col-md-12 cleaner">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Puntos de muestreo:</label>
                                <div class="form-group-input cleaner">
                                    <textarea class="form-textarea" maxlength="20"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

            </section>
        </section>
    </main>
    
    <?php include '../_include/footer.php' ?>
    
</body>
</html>