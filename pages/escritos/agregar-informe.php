<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="../encuestadora/perfil.css">
    <link rel="stylesheet" href="agregar.css">
    <title>AGREGAR INFORME DE ENCUESTA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">Agregar Informes de Ecuestas</h1>
                </div>

                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Datos Generales</div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Departamento: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Provincia: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-4 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Distrito: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-12 cleaner">
                                <div class="cleaner resumen-subtitle">Domicilio: <span class="resumen-subtitle--is-detail">Mz S Lote 1 Calle las Moras Urb Ceres Ate</span></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Financiamiento </div>
                    <div class="cleaner resumen-detail-content">
                        <div class="row-fluid">
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Teléfono: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Celular: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">P. Web: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            <div class="col-md-3 col-sm-6 cleaner m-b-20">
                                <div class="cleaner resumen-subtitle">Correo: <span class="resumen-subtitle--is-detail">Lima</span></div>
                            </div>
                            
                        </div>
                    </div>
                </div>


                <div class="box-content m-t-30">
                    <div class="resumen-subtitle resumen-subtitle--is-txt m-b-20">Archivos </div>
                    <div class="row-fluid cleaner">
                        <div class="col-md-6 cleaner">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Archivo a subir:</label>
                                <label class="form-group-select " for="departamento">
                                    <select id="departamento" name="options">
                                        <option value="">--Seleccione--</option>
                                        <option value="1">San Marcos</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 cleaner">
                            <div class="form-group cleaner m-b-20">
                                <label for="User" class="form-label">Tipo de descripción:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="archivos-subir-texto">  
                        <label for="User" class="form-label">Base de datos:</label>
                    </div> 
                    
                    <upload id="uploadMultiple"></upload>
                    <script type="text/html" id="uploadTemplate">
                        <form class="upload-multiple">
                            <span class="inputs"></span>
                            <p class="uploadCaption">
                                Arrastra tus archivos o da click <span class="link">aquí</span> para agregarlos.
                            </p>
                            <p class="subcaption">Tamaño máximo: 10Mb | Solo tipos: jpg, pdf, excel</p>
                            <section id="uploadItems"></section>
                        </form>
                    </script>

                </div>

            </section>
        </section>
    </main>
    
    
    <?php include '../_include/footer.php' ?>
    <script src="../../js/uploadfile.js"></script>
    <script>
        new FileUploader("uploadMultiple", "uploadTemplate");
    </script>
    
</body>
</html>