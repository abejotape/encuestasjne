<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="visar-informe.css">
    <title>ADMINISTRAR INFORME DE ENCUESTA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">Administrar informe de Encuestas</h1>
                </div>


                <div class="box-content">

                    <div class="message-inline message-inline--is-info cleaner">
                        <i class="message-inline-icon material-icons">info</i> 
                        <p class="message-inline-text">De acuerdo al reglamento de XXXXXXX se debe considerar información mínima en el informe. Hasta ahora ha cumplido con la siguiente información. Por favor marcar aquellas secciones que están contemplados en el informe</p>
                    </div>

                    <ul class="checks-content cleaner">
                       <li class="checks-content-li">
                           <span>01.Fecha de realización </span>
                           <label for="checkbox-test" class="form-checkbox checks-content-checkbox">
                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                            </label>
                       </li>
                       <li class="checks-content-li">
                           <span>02. Fecha de publicación o difusión del estudio </span>
                           <label for="checkbox-test" class="form-checkbox checks-content-checkbox">
                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                            </label>
                       </li>
                   </ul>

                   <div class="button-content cleaner">
                        <button class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                        <button class="button button-primary" id="prueba"><i class="material-icons">send</i>Aceptar</button>
                    </div>  

                </div>
            </section>
        </section>
    </main>
    
    <?php include '../_include/footer.php' ?>
    
</body>
</html>