<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="visar-informe.css">
    <title>VISAR INFORME DE ENCUESTA</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="titles-content">
                    <h1 class="title-primary">Agregar informe de Encuestas</h1>
                </div>


                <div class="box-content">
                    
                    <div class="visar cleaner">
                    	<div class="visar-datos">
                    		<div class="row-fluid">
                    			<div class="col-md-6 m-b-20">
                    				<label class="visar-title">Fecha d estudio</label>
	                                <div class="form-group-input cleaner">
	                                    <label class="visar-detail">12/10/2019</label>
	                                </div>
                    			</div>
                    			<div class="col-md-6 m-b-20">
                    				<label class="visar-title">Fecha de publicación</label>
	                                <div class="form-group-input cleaner">
	                                    <label class="visar-detail">12/10/2019</label>
	                                </div>
                    			</div>
                    			<div class="col-md-12 m-b-20">
                    				<label class="visar-title">Objetivos</label>
	                                <div class="form-group-input cleaner">
	                                    <label class="visar-detail ">El estudio también reveló que un contundente 72 % de
peruanos está en contra de la vacancia presidencial, moción que el aprista Mauricio Mulder piensa presentar porque, según
él, Vizcarra no</label>
	                                </div>
                    			</div>

                    			<div class="col-md-6 m-b-20">
                    				<label class="visar-title">Ámbito</label>
	                                <div class="form-group-input cleaner">
	                                    <label class="visar-detail">Rural y urbano</label>
	                                </div>
                    			</div>
                    			<div class="col-md-6 m-b-20">
                    				<label class="visar-title">Población</label>
	                                <div class="form-group-input cleaner">
	                                    <label class="visar-detail">Rural y urbano</label>
	                                </div>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="visar-preview">
                    		<iframe width="100%" height="315" src="https://www.ipsos.com/sites/default/files/ct/publication/documents/2019-01/las_encuestas_top_del_2018.pdf" frameborder="0" allowfullscreen></iframe>

                    	</div>
                    </div>



                </div>

            </section>
        </section>
    </main>
    
    <?php include '../_include/footer.php' ?>
    
</body>
</html>