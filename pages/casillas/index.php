<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <title>CASILLAS ELECTRÓNICAS</title>
</head>
<body>
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="inbox cleaner">
                    <div class="inbox-header">
                        <div class="layout-title">Casillas Electrónicas</div>
                        <div class="inbox-header-button"><p>Etiquetar</p></div>
                        <div class="inbox-header-button"><p>Enviar</p></div>
                        <div class="inbox-header-button"><p>Archivar</p></div>
                        <div onclick="(function(){ fn.modal.open('wc-modal-crear-casilla') })()" class="inbox-header-button inbox-header-button--is-send cleaner"><i class="material-icons">add</i> Agregar</div>        
                    </div>
                    <div class="inbox-body">
                        <table class="inbox-table cleaner">
                            <thead class="inbox-table-thead">
                                <tr class="inbox-table-thead-tr">
                                    <td class="inbox-table-thead-td">
                                        <label for="checkbox-test" class="form-checkbox">
                                            <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test">
                                        </label>
                                    </td>
                                    <td class="inbox-table-thead-td">N° Casilla</td>
                                    <td class="inbox-table-thead-td">Nombres</td>
                                    <td class="inbox-table-thead-td">Apellidos</td>
                                    <td class="inbox-table-thead-td">Fecha registro</td>
                                    <td class="inbox-table-thead-td">Envío correo</td>
                                    <td class="inbox-table-thead-td">Envío SMS</td>
                                    <td class="inbox-table-thead-td">Estado</td>
                                    <td class="inbox-table-thead-td">...</td>
                                </tr>
                            </thead>
                            <tbody class="inbox-table-tbody">

                                <!-- Fila con su detalle -->
                                <tr id="test" class="inbox-table-tbody-tr inbox-table-odd">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">4033321</a></td>
                                    <td class="inbox-table-tbody-td">Daniel Jose Armando</td>
                                    <td class="inbox-table-tbody-td">Acevedo Jhong</td>
                                    <td class="inbox-table-tbody-td">21/01/2019</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">Si</td>
                                    <td class="inbox-table-tbody-td">Activo</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()" >
                                            <li class="table-options-li">Editar</li>
                                            <li class="table-options-li">Borrar</li>
                                        </ul>
                                    </td>
                                </tr>
                                
                                <!-- end Fila con su detalle -->

                                <tr class="inbox-table-tbody-tr inbox-table-even">
                                    <td class="inbox-table-tbody-td" style="width:70px">
                                        <div class="p-r">
                                            <label for="checkbox-test" class="form-checkbox">
                                                <input type="radio" class="form-checkbox-input" name="checkbox-test" id="checkbox-test" checked>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="inbox-table-tbody-td"><a href="#" class="link">43233242</a></td>
                                    <td class="inbox-table-tbody-td">Miguel Feliciano</td>
                                    <td class="inbox-table-tbody-td">Rodriguez de la Rosa</td>
                                    <td class="inbox-table-tbody-td">21/01/2019</td>
                                    <td class="inbox-table-tbody-td">No</td>
                                    <td class="inbox-table-tbody-td">No</td>
                                    <td class="inbox-table-tbody-td">Generado</td>
                                    <td class="inbox-table-tbody-td table-options">
                                        <div class="table-icons">
                                            <i class="material-icons table-icons--is-icon">more_vert</i>
                                        </div>
                                        <ul class="table-options-ul" onmouseleave="(function(){ fn.actions.hide(event) })()">
                                            <li class="table-options-li">Editar</li>
                                            <li class="table-options-li">Borrar</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </section>
    </main>

    <!-- Modal Crear documento -->
    <wc-modal-crear-casilla class="modal-content modal-content--is-small cleaner" >
        <form action="" class="formulario cleaner">
            <a onclick="(function(){ fn.modal.close('wc-modal-crear-casilla') })()" class="modal-content--is-close cleaner" href="javascript:void(0)"><i class="material-icons modal-content-icon--is-close">cancel</i></a>
    
            <div class="modal-head cleaner">
                <strong class="modal-head-title">Agregar Casilla electrónica</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid cleaner">
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner">
                            <label for="User" class="form-label">Nro. Casilla</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Nombres</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Apellidos</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">                
                    <div class="col-sm-3 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Sede electoral:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Seleccione--</option>
                                    <option value="1">San Marcos</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Habilitado:</label>
                            <div class="form-group-input cleaner">
                                <label for="checkbox-test" class="form-checkbox">
                                    <input type="checkbox" class="form-checkbox-input" name="checkbox-test" id="checkbox-test">
                                    <label for="" class="form-radio-label">Si</label>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="button-content cleaner">
                <button onclick="(function(){ fn.modal.close('wc-modal-crear-casilla') })()" type="button" class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                <button type="button" class="button button-primary" ><i class="material-icons">highlight_off</i>Guardar</button>
            </div>
        </form>
    </wc-modal-crear-casilla>

    <?php include '../_include/footer.php' ?>
    
</body>
</html>